import itertools
import re
import pytest
import yaml

from src.parse_yaml import loader_factory

from src.yaml_spec import YamlFormat, WorkerConstructor


@pytest.fixture()
def yfmt():
    """ Yaml specification instance
    """
    return YamlFormat()


def test_match_braces(yfmt):
    string = '{eb.configure_flags_min} {eb.configure_flags_blas} --with-fftw-prefix=${EBROOTFFTW}'
    matches = re.findall(yfmt.regex, string)
    assert matches == ['eb.configure_flags_min', 'eb.configure_flags_blas'], \
        'Match anything between {}. but ignore ${...}'


def test_match_generic_library_variable(yfmt):
    string = "--with-libxc-prefix=$PMROOT_LIBXC+ --with-gsl-prefix=$PMROOT_GSL"
    matches = re.findall(yfmt.library_var_regex, string)
    assert matches == ['PMROOT_LIBXC', 'PMROOT_GSL']


def test_extract_lib(yfmt):
    generic_lib_var = 'PMROOT_ETSF_IO'
    lib = yfmt.extract_lib(generic_lib_var)
    assert lib == 'ETSF_IO'


def test_set_eb_library_variable_name(yfmt):
    matches = ['PMROOT_ETSF_IO', 'PMROOT_LIBXC', 'PMROOT_GSL']
    new_variables = yfmt.set_eb_library_variable_name(matches)
    assert new_variables == ['EBROOTETSF_IO', 'EBROOTLIBXC', 'EBROOTGSL']


def test_set_spack_library_variable_name(yfmt):
    matches = ['PMROOT_ETSF_IO', 'PMROOT_LIBXC', 'PMROOT_GSL', 'PMROOT_ATLAB']
    new_variables = yfmt.set_spack_library_variable_name(matches)
    assert new_variables == ['MPSD_ETSF_IO_ROOT', 'MPSD_LIBXC_ROOT', 'MPSD_GSL_ROOT', 'MPSD_BIGDFT_ATLAB_ROOT']


def test_worker_constructor():
    yaml_str = """
    workers:
      - !WorkerGroup tentacles
      - 'extra-tentacle'
    """
    workers = {'tentacles': ['tentacle1', 'tentacle2', 'tentacle3']}

    worker_constructor = WorkerConstructor(workers)
    custom_loader = loader_factory(worker_constructor)
    data = yaml.load(yaml_str, Loader=custom_loader)

    assert data == {'workers': [['tentacle1', 'tentacle2', 'tentacle3'], 'extra-tentacle']}
