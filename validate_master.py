"""
This script is used to debug the buildbot configuration file.
It is equivalent to calling `buildbot checkconfig` from the command line.
"""
from buildbot.config.errors import ConfigErrors
from buildbot.config.master import FileLoader
import os
import sys


config_file = 'master.cfg'
try:
    FileLoader(os.getcwd(), config_file).loadConfig()
except ConfigErrors as e:
    raise ValueError("Configuration Errors:", e.errors)

print("Config file is good!")