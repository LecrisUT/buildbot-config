""" Functions that comprise Octopus VPath Builder
"""
from copy import deepcopy
import re
from typing import Tuple, List, Optional, Union, Callable

from buildbot.plugins import util, steps
from buildbot.process.results import SKIPPED
from buildbot.process.buildstep import BuildStep
from buildbot.config import BuilderConfig

from src.builder import PackageManager
from src.custom_steps import GitLabStep, BBGitStep, OctopusTestingStep, OctopusCodeCovUploadStep, \
    OctopusTestPerformanceStep, OctopusPerformanceUploadStep, OctopusTestUploadStep, ValgrindTestingStep
from src.url_builder import url_factory


def initialise_env(var: dict) -> dict:
    """ Initialise env with var.

    Note, all values in the env dict must be strings, so enforce in this function.

    TODO(Alex) Issue 34. Move env defaults to builders.yaml, then remove this function
    Which would reduce this to myenv = deepcopy({key: str(value) for key, value in var.items()})

    :param var: Builder environment variables.
    :return: myenv: Environment dictionary with defaults + var key,values.
    """
    myenv = {"LOCAL_MAKEFLAGS": "-j", "OMP_NUM_THREADS": "1", "LC_ALL": "C.UTF-8"}
    myenv.update({key: str(value) for key, value in var.items()})
    return myenv


def add_code_coverage_to_env(myenv: dict) -> dict:
    """ Add code coverage to environment dictionary.

    :param myenv: Environment dictionary.
    :return: env: Environment dictionary with code cov flags added.
    """
    env = deepcopy(myenv)
    coverage_str = '-ftest-coverage -fprofile-arcs'
    keys = set(env)
    # If present in env
    flags_to_append = {"CFLAGS", "CXXFLAGS", "FCFLAGS"}.intersection(keys)
    # If not present in env
    flags_to_initialise = {"CFLAGS", "CXXFLAGS", "FCFLAGS"} - flags_to_append

    for flag in flags_to_append:
        env[flag] += ' ' + coverage_str

    for flag in flags_to_initialise:
        env[flag] = coverage_str

    return env


def set_toolchain_variant(variant: str, cuda: bool, mpi: bool, valgrind: bool) -> str:
    """ Set toolchain variant string.

    :param variant: Variant type.
    :param mpi: Is MPI.
    :param cuda: Is CUDA.
    :param valgrind: Use valgrind
    :return: Toolchain variant string.
    """
    d_cuda = {True: '-CUDA', False: ''}
    d_mpi = {True: '-mpi', False: ''}
    d_valgrind = {True: " Valgrind", False: ''}
    return variant + d_cuda[cuda] + d_mpi[mpi] + d_valgrind[valgrind]


def eb_module_command(modulecmd: str, toolchain: str, version: str, toolchain_variant: str) -> str:
    """ Define EasyBuild module command.

    :param modulecmd: Module command prefix (path/to/Lua, for example)
    :param toolchain: Toolchain
    :param version: Toolchain version
    :param toolchain_variant: Toolchain variant
    :return: modules_command: Full module command, appended with `&&`, implying `modules_command`
    MUST be appended in a subsequent call.
    """
    modules_command = "eval `" + modulecmd + " sh use $EASYBUILD_PREFIX/" + version + "/modules/Core ` && "
    modules_command = modules_command + "eval `" + modulecmd + " sh load " + toolchain + " Octopus/" + toolchain_variant
    # This assumes additional string appends follow
    modules_command = modules_command + " ` && "
    return modules_command


def get_spack_toolchain_name(toolchain: str, version: str, toolchain_variant: str) -> str:
    """ Define Spack toolchain name.

    Does the following:

    - drop the words "full" "min" from the toolchain variant
    as it is not part of the spack toolchain name
    this esssentially means that the
    variant variable in `set_toolchain_variant` is ignored
    - replace CUDA with cuda
    - default to serial if no variant is left. This is the case for spack toolchains
    as we explicitly specify 'serial' in the toolchain name but not so in the EasyBuild toolchains
    :param toolchain: Toolchain (eg. foss)
    :param version: Toolchain version (eg. 2021a)
    :param toolchain_variant: Toolchain variant (eg. min,full-mpi)
    :return: spack_toolchain_name: Spack toolchain name  (eg. foss2021a-mpi)
    """
    spack_toolchain_prefix = re.sub(r'(full|min)', '', toolchain_variant)
    spack_toolchain_prefix = spack_toolchain_prefix.replace("CUDA", "cuda")
    if spack_toolchain_prefix == "":
        spack_toolchain_prefix = "-serial"

    spack_toolchain_name = toolchain + version + spack_toolchain_prefix
    return spack_toolchain_name


def spack_module_command(modulecmd: str, toolchain: str, version: str, toolchain_variant: str) -> str:
    """ Define Spack module command.

    :param modulecmd: Module command prefix (path/to/Lua, for example)
    :param toolchain: Toolchain (eg. foss)
    :param version: Toolchain version (eg. 2021a)
    :param toolchain_variant: Toolchain variant (eg. min,full-mpi)
    :return: modules_command: Full module command, appended with `&&`, implying `modules_command`
    """
    modules_command = f"eval `{modulecmd} sh use /opt_mpsd/linux-debian11/23b/sandybridge/lmod/Core` && "

    spack_toolchain_name = get_spack_toolchain_name(toolchain, version, toolchain_variant)
    modules_command = modules_command + f"eval `{modulecmd} sh load toolchains/{spack_toolchain_name}` && "
    # spack doesn't set rpath, so we need to set LDFLAGS manually
    modules_command = modules_command + r"export LDFLAGS=`echo ${LIBRARY_PATH:+:$LIBRARY_PATH} | sed -e 's/:/ -Wl," \
                                        r"-rpath=/g'` && "
    modules_command = modules_command + r"export BLAS_LIB_DIR=$MPSD_OPENBLAS_ROOT/lib && "
    return modules_command


def module_command(package_manager: PackageManager) -> Callable:
    """ Choose which module command function to use

    :param package_manager: Package manager
    :return cmd_func: Function defining the module command
    """
    cmd_func = {PackageManager.EB: eb_module_command,
                PackageManager.SPACK: spack_module_command}
    try:
        return cmd_func[package_manager]
    except KeyError:
        raise KeyError(f'Package manager not recognised: {package_manager.name}\n'
                       f'Valid choices are {PackageManager.names()}.')


def export_env_flags(modules_command: str, myenv: dict) -> Tuple[str, dict]:
    """ Export specific environment flags.
    The following flags need to be exported _after_ loading the modules

    :param modulescommand: Module commabd.
    :param myenv: Environment variables.
    :return: Tuple of updated modules command and environment variables
    """
    cmd = modules_command
    env = deepcopy(myenv)
    all_exportable_flags = ["CC", "CXX", "FC", "CFLAGS", "CXXFLAGS", "FCFLAGS",
                            "FCFLAGS_ELPA", "FCFLAGS_FFTW", "MPIEXEC"]
    flags_to_export = [f for f in list(myenv) if f in all_exportable_flags]

    for variable in flags_to_export:
        cmd += f'export {variable}="{env.pop(variable)}" && '

    return cmd, env


def run_tests(step: BuildStep) -> bool:
    """ Determine whether to run tests.

    :param step: Buildbot build step
    :return: Whether to run tests
    """
    if step.build.getProperty('skip_tests', default=False):
        return False
    return True


def octopus_autotools_builder(
        buildname: str,
        workers: Union[str, List[str]],
        toolchain: str,
        version: str,
        var: dict,
        flags: Union[str, List[str]],
        variant='full',
        mpi=False,
        cuda=False,
        test='full',
        timeout=1800,
        testCommand=None,
        modulecmd='/usr/share/lmod/lmod/libexec/lmod',
        codeCoverage=False,
        valgrind=False,
        failOnCompilerWarnings=False,
        package_manager: Optional[PackageManager] = PackageManager.EB) -> BuilderConfig:
    """ Set up an Octopus build in a VPATH environment.

    :param buildname: Build name.
    :param workers: List of workers.
    :param toolchain: Tool chain. For example: 'foss', 'intel'.
    :param version: Tool chain version. For example: '2020a', '2020b'.
    :param var: Variables to export, including C, C++ and fortran compiler flags.
    :param flags: All other flags.
    :param variant: Toolchain variant override. For example: 'warnings', 'min', 'debug'.
    :param mpi: Optional. Use MPI version of toolchain.
    :param cuda: Optional. Use CUDA version of toolchain.
    :param test: Optional. Test type. See function body for valid options.
    :param timeout: Optional. Builder time-out in seconds.
    :param testCommand: Optional. Test command. If not passed, will be determined by the
    OctopusTestingStep class. Note, this option is never passed to a builder in master.cfg
    :param modulecmd: Lua module 'lmod' location.
    :param codeCoverage: Optional. Run code coverage step.
    :param valgrind: Optional. Run valgrind step.
    :param failOnCompilerWarnings: Optional. Builder fails if a warning is raised.
    :param package_manager: Choice of package manager. Only added as optional to avoid breaking
    the existing API.
    :return: A builder configuration instance.
    """
    valid_tests = ['short', 'long', 'full', 'dist', 'distcheck', 'performance', 'none']
    if test not in valid_tests:
        raise ValueError(f'test value {test} is not valid.\n'
                         f'Valid choices are {valid_tests}')

    # Set some default environment variables and copy the environment
    # variables passed as arguments to myenv.
    myenv = initialise_env(var)

    # Add compiler flags for code coverage
    if codeCoverage:
        assert toolchain != 'intel', 'code coverage only works for GCC'
        myenv = add_code_coverage_to_env(myenv)

    # Set up command to load environment modules
    toolchain_variant = set_toolchain_variant(variant, cuda, mpi, valgrind)

    modulescommand = module_command(package_manager)(modulecmd, toolchain, version, toolchain_variant)
    modulescommand, myenv = export_env_flags(modulescommand, myenv)

    # Create the flag string
    # Note, definitions in master.cfg will be List[str]
    # but those returned from yml are already concatenated, so do nothing in that instance
    if isinstance(flags, List):
        flagstring = " ".join(['"' + s + '"' for s in flags])
    else:
        flagstring = flags

    # Create list of workers
    myworkers = workers if isinstance(workers, list) else [workers]

    # URL instance
    url = url_factory()

    # Now we create the actual factory
    b = util.BuildFactory()
    b.addStep(GitLabStep)
    b.addStep(BBGitStep)

    # The chmod is necessary because files are left read-only if a distcheck fails.
    # rm -rf is not enough to remove them then.
    b.addStep(steps.ShellCommand(description="cleaning",
                                 descriptionDone="clean",
                                 command="if [ -d _build ] ; then chmod -R +w _build && rm -rf _build; fi",
                                 haltOnFailure=True))

    b.addStep(steps.ShellCommand(description="generating configure",
                                 descriptionDone="generate configure",
                                 command=modulescommand + 'autoreconf -i',
                                 haltOnFailure=True))

    b.addStep(steps.Configure(command=modulescommand + 'mkdir _build && cd _build && ../configure ' + flagstring,
                              logfiles={"config.log": "_build/config.log",
                                        "config.h": "_build/config.h"},
                              haltOnFailure=True))

    if test == "distcheck":
        myenv.update({"DISTCHECK_CONFIGURE_FLAGS": flagstring, "SKIP_CHECK": "yes"})
        b.addStep(steps.ShellCommand(description="distchecking",
                                     descriptionDone="distcheck",
                                     command=modulescommand + "cd _build && make distcheck"))

    if test == 'dist':
        # TODO(Alex) Issue 35. Refactor dist command to a custom step class
        myenv['DISTCHECK_CONFIGURE_FLAGS'] = flagstring
        myenv['SKIP_CHECK'] = 'yes'
        b.addStep(steps.ShellCommand(description='dist',
                                     descriptionDone='dist',
                                     command=modulescommand + 'cd _build && make dist'))
        b.addStep(steps.SetPropertyFromCommand(command='cd _build && ls octopus-*.tar.gz', property='tarball'))
        path = url.webrootfolder() + 'download'
        b.addStep(steps.MasterShellCommand(command=util.Interpolate('mkdir -p ' + path + '/%(src::branch)s/')))
        if util.Interpolate('/%(src::branch)s') == 'main':
            masterdestination = '/main/octopus-main.tar.gz'
        else:
            masterdestination = util.Interpolate('/%(src::branch)s/%(prop:tarball)s')
        b.addStep(steps.FileUpload(workersrc=util.Interpolate('_build/%(prop:tarball)s'),
                                   masterdest=path+masterdestination,
                                   url=url.title() + '/download' + masterdestination))
        b.addStep(steps.MasterShellCommand(command=util.Interpolate(
            'chmod 0755 ' + path + '/%(src::branch)s && chmod -R 0644 ' + path + masterdestination)))

    if not (test == 'dist' or test == 'distcheck'):
        b.addStep(steps.Compile(description="compiling",
                                descriptionDone="compile",
                                command=modulescommand + "cd _build && make clean && make $LOCAL_MAKEFLAGS all",
                                flunkOnWarnings=failOnCompilerWarnings,
                                haltOnFailure=True))

    if test == 'performance':
        b.addStep(OctopusTestPerformanceStep(module_command=modulescommand,
                                             timeout=timeout,
                                             doStepIf=run_tests))

        b.addStep(OctopusPerformanceUploadStep(timeout=timeout, doStepIf=run_tests))

    if test in ['short', 'long', 'full']:

        if valgrind:
            b.addStep(ValgrindTestingStep(modules_command=modulescommand,
                                          test=test,
                                          package_manager=package_manager,
                                          test_command=testCommand,
                                          timeout=timeout,
                                          doStepIf=run_tests))
        else:
            b.addStep(OctopusTestingStep(modules_command=modulescommand,
                                         test=test,
                                         test_command=testCommand,
                                         timeout=timeout,
                                         doStepIf=run_tests))

        b.addStep(OctopusTestUploadStep(timeout=timeout, doStepIf=run_tests))

        if codeCoverage:
            b.addStep(OctopusCodeCovUploadStep(modules_command=modulescommand,
                                               workdir="build/_build/src",
                                               doStepIf=run_tests
                                               ))

    return util.BuilderConfig(name=buildname, workernames=myworkers, factory=b, env=myenv)
