from src.autotools_builder import initialise_env, add_code_coverage_to_env, set_toolchain_variant, eb_module_command, \
    export_env_flags, spack_module_command, get_spack_toolchain_name


def test_initialise_env():
    # Pass empty dict
    env = initialise_env({})
    default_env = {"LOCAL_MAKEFLAGS": "-j", "OMP_NUM_THREADS": "1", "LC_ALL": "C.UTF-8"}
    assert env == default_env, 'Only contains values defined in `initialise_env`'

    # Pass some standard-looking FOSS flags
    var = {'CFLAGS': '-Wall -O2 -march=native',
           'CXXFLAGS': '-Wall -O2 -march=native',
           'FCFLAGS': '-O2 -march=native -fbacktrace',
           'LOCAL_MAKEFLAGS': '-j 16',
           'OCT_TEST_NJOBS': '16'
           }
    env = initialise_env(var)
    ref_env = {**default_env, **var}
    assert env == ref_env


def test_add_code_coverage_to_env():
    # Pass empty dict
    output_env = add_code_coverage_to_env({})
    ref_env = {'CFLAGS': '-ftest-coverage -fprofile-arcs',
               'CXXFLAGS': '-ftest-coverage -fprofile-arcs',
               'FCFLAGS': '-ftest-coverage -fprofile-arcs'
               }

    assert set(output_env) == {'CFLAGS', 'CXXFLAGS', 'FCFLAGS'}, 'relevant flag keys added to env'
    assert output_env == ref_env, "Adds code cov flags to relevant compiler flags"

    # Pass some standard-looking FOSS flags
    var = {'CFLAGS': '-Wall -O2 -march=native',
           'CXXFLAGS': '-Wall -O2 -march=native',
           'FCFLAGS': '-O2 -march=native -fbacktrace',
           'LOCAL_MAKEFLAGS': '-j 16',
           'OCT_TEST_NJOBS': '16'
           }

    output_env = add_code_coverage_to_env(var)

    ref_env = {'CFLAGS': '-Wall -O2 -march=native -ftest-coverage -fprofile-arcs',
               'CXXFLAGS': '-Wall -O2 -march=native -ftest-coverage -fprofile-arcs',
               'FCFLAGS': '-O2 -march=native -fbacktrace -ftest-coverage -fprofile-arcs',
               'LOCAL_MAKEFLAGS': '-j 16',
               'OCT_TEST_NJOBS': '16'
               }

    assert output_env == ref_env, 'Code cov flags appended to existing CFLAGS, CXXFLAGS and FCFLAGS'


def test_set_toolchain():
    variant = 'full'
    toolchain_variant = set_toolchain_variant(variant, False, False, False)
    assert toolchain_variant == variant

    toolchain_variant = set_toolchain_variant(variant, True, True, True)
    assert toolchain_variant == 'full-CUDA-mpi Valgrind'


def test_eb_module_command():
    module_cmd = 'lmod'
    toolchain = 'foss'
    version = 'foss-2020b_mpi'
    toolchain_variant = 'full'

    cmd = eb_module_command(module_cmd, toolchain, version, toolchain_variant)
    expected_cmd = "eval `lmod sh use $EASYBUILD_PREFIX/foss-2020b_mpi/modules/Core ` && " \
                   "eval `lmod sh load foss Octopus/full ` && "
    assert cmd == expected_cmd

    toolchain_variant = set_toolchain_variant('min', True, True, True)
    cmd = eb_module_command(module_cmd, toolchain, version, toolchain_variant)
    expected_cmd = "eval `lmod sh use $EASYBUILD_PREFIX/foss-2020b_mpi/modules/Core ` && " \
                   "eval `lmod sh load foss Octopus/min-CUDA-mpi Valgrind ` && "
    assert cmd == expected_cmd


def test_get_spack_toolchain_name():
    assert get_spack_toolchain_name('foss', '2022a', 'full-mpi') == 'foss2022a-mpi'
    assert get_spack_toolchain_name('foss', '2021a', 'full') == 'foss2021a-serial'
    assert get_spack_toolchain_name('foss', '2022a', 'full-CUDA-mpi') == 'foss2022a-cuda-mpi'


def test_spack_module_command():
    module_cmd = 'lmod'
    toolchain = 'foss'
    version = '2022a'
    toolchain_base_variant = 'full'

    # test full, mpi variant
    toolchain_variant = set_toolchain_variant(toolchain_base_variant, False, True, False)
    cmd = spack_module_command(module_cmd, toolchain, version, toolchain_variant)
    # Create the expected command
    module_use_cmd = f"eval `lmod sh use /opt_mpsd/linux-debian11/23b/sandybridge/lmod/Core` && "
    module_load_cmd = f"eval `lmod sh load toolchains/foss2022a-mpi` && "
    export_cmd = r"export LDFLAGS=`echo ${LIBRARY_PATH:+:$LIBRARY_PATH} | sed -e 's/:/ -Wl,-rpath=/g'` && "
    export_cmd += r"export BLAS_LIB_DIR=$MPSD_OPENBLAS_ROOT/lib && "
    assert cmd == module_use_cmd + module_load_cmd + export_cmd

    # test full, serial variant
    toolchain_variant = set_toolchain_variant(toolchain_base_variant, False, False, False)
    cmd = spack_module_command(module_cmd, toolchain, version, toolchain_variant)
    # Create the expected command
    module_load_cmd = f"eval `lmod sh load toolchains/foss2022a-serial` && "
    expected_cmd = module_use_cmd + module_load_cmd + export_cmd
    assert cmd == expected_cmd

    # test full + cuda, mpi variant
    toolchain_variant = set_toolchain_variant(toolchain_base_variant, True, True, False)
    cmd = spack_module_command(module_cmd, toolchain, version, toolchain_variant)
    # Create the expected command
    module_load_cmd = f"eval `lmod sh load toolchains/foss2022a-cuda-mpi` && "
    expected_cmd = module_use_cmd + module_load_cmd + export_cmd
    assert cmd == expected_cmd


def test_export_env_flags():
    # Inputs
    modules_command = "eval `lmod sh use $EASYBUILD_PREFIX/foss-2020b_mpi/modules/Core ` && " \
                      "eval `lmod sh load foss Octopus/full ` && "

    env = {'CFLAGS': '-Wall -O2 -march=native',
           'CXXFLAGS': '-Wall -O2 -march=native',
           'FCFLAGS': '-O2 -march=native -fbacktrace',
           'LOCAL_MAKEFLAGS': '-j 16',
           'OCT_TEST_NJOBS': '16'
           }

    # Expected outputs
    ref_cmd = 'eval `lmod sh use $EASYBUILD_PREFIX/foss-2020b_mpi/modules/Core ` && ' \
              'eval `lmod sh load foss Octopus/full ` && ' \
              'export CFLAGS="-Wall -O2 -march=native" && ' \
              'export CXXFLAGS="-Wall -O2 -march=native" && ' \
              'export FCFLAGS="-O2 -march=native -fbacktrace" && '

    ref_env = {'LOCAL_MAKEFLAGS': '-j 16',
               'OCT_TEST_NJOBS': '16'
               }

    output_cmd, output_env = export_env_flags(modules_command, env)

    assert output_cmd == ref_cmd, "Compiler flags present in env exported in module command"
    assert output_env == ref_env, "Exported flags removed from the environment dictionary"
