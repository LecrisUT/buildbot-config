""" Yaml specifications
"""
from __future__ import annotations
from dataclasses import dataclass
import enum
from pathlib import Path
from typing import List, Callable
import yaml

from src.parse_yaml import AbstractConstructor, loader_factory


class PackageManager(enum.Enum):
    """Valid package managers"""
    EB = enum.auto()
    SPACK = enum.auto()

    @classmethod
    def names(cls) -> List[str]:
        """Get a list of names
        :return List of names
        """
        return [pm.name for pm in cls]


@dataclass
class YamlFormat:
    """ Define the specific notation used in the builders YAML
    file to define variables.
    """
    # File name
    file_name = "builders.yaml"

    # Regex matches between braces, {VAR}, using \{(.*?)\
    # but will ignore ${VAR}, using (?<!\$)
    regex = r'(?<!\$)\{(.*?)\}'

    # Delimiter used in flag variable definition
    delimiter: str = '.'

    # Generic library prefix
    generic_lib_prefix = 'PMROOT'

    # regex to match library variable names
    # i.e. match $PMROOT_LIBXC, return PMROOT_LIBXC
    library_var_regex = rf"\$({generic_lib_prefix}_[^\s\W]+)"

    # Valid toolchains
    valid_toolchains = {'foss', 'intel'}

    # Valid top-level keys
    valid_toplevel_keys = {'environment_variables', 'flags', 'optional_builder_defaults', 'EB', 'SPACK'}

    @staticmethod
    def variable(x: str) -> str:
        """Flag variable definition in YAML.
        """
        return '{' + x + '}'

    def keys(self, key_string: str) -> List[str]:
        """ Create a list of keys from a concatenated string.
        i.e. Input 'a.b.c.' and return ['a', 'b', 'c']
        """
        return key_string.split(self.delimiter)

    def libvar_name_setter(self, package_manager: PackageManager) -> Callable[[List[str]], List[str]]:
        """ Return a function defining library variables, for a given package manager.

        The library variable naming convention differs between package managers. For example:
         Package manager       Variable
            EASYBUILD         EBROOTLIBXC
            SPACK             MPSD_LIBXC_ROOT

        :param package_manager: Package manager
        :return Function that accepts the generic library variable str and returns
        the package manager-specific library variable str.
        """
        lib_var_name = {PackageManager.EB: self.set_eb_library_variable_name,
                        PackageManager.SPACK: self.set_spack_library_variable_name}
        try:
            return lib_var_name[package_manager]
        except KeyError:
            raise KeyError(f'Package manager not recognised: {package_manager.name}\n'
                             f'Valid choices are {PackageManager.names()}.')

    def extract_lib(self, lib_var: str) -> str:
        """ Extract library name from library variable.

        Convention of generic library variables is:
        'PMROOT_LIBNAME'
        Return 'LIBNAME'

        Note, a simple implementation like `lib_var.split('_')[-1]`
        will fail for a variable like 'PMROOT_ETSF_IO'
        """
        prefix = self.generic_lib_prefix + '_'
        try:
            i_start = lib_var.index(prefix)
        except ValueError:
            raise ValueError(f'"{prefix}" not in library variable {lib_var}')

        i = i_start + len(prefix)
        return lib_var[i:]

    def handle_spack_exceptions(self, lib_var: str) -> str:
        """ Handle special cases for library variable names.

        For example, the library variable 'MPSD_NETCDF_ROOT' should be mapped to 'MPSD_NETCDF_FORTRAN_ROOT'
        """
        exception_libraries = {
            'ATLAB':'BIGDFT_ATLAB',
            'FUTILE': 'BIGDFT_FUTILE',
            'PSOLVER': 'BIGDFT_PSOLVER',
            'SCALAPACK':'NETLIB_SCALAPACK',
            'NETCDF':'NETCDF_FORTRAN',
        }
        if lib_var in exception_libraries.keys():
            return exception_libraries[lib_var]

        return lib_var

    def set_eb_library_variable_name(self, lib_variables: List[str]) -> List[str]:
        return ['EBROOT' + self.extract_lib(var) for var in lib_variables]

    def set_spack_library_variable_name(self, lib_variables: List[str]) -> List[str]:
        return ['MPSD_' + self.handle_spack_exceptions(self.extract_lib(var)) + '_ROOT' for var in lib_variables]


class WorkerConstructor(AbstractConstructor):
    """ Custom YAML constructor class for the `workers` entry of a builder.

    For example:
    ```yaml
    foss:
      2020b_mpi:
        workers:
         -  !WorkerGroup tentacles
         - 'tentacles1'
         - 'tentacles2'
         - 'tentacles3'
    ```
    will be parsed as:

    builder['foss']['2020b_mpi']['workers'] = [workers['tentacles'], 'tentacles1', 'tentacles2', 'tentacles3']
    """
    # pyaml tag
    tag = '!WorkerGroup'

    def __init__(self, workers: dict):
        self.workers = workers

    def constructor(self, loader: yaml.SafeLoader, node: yaml.nodes.ScalarNode) -> List[str]:
        """ Custom YAML Constructor.

        If the tag is present, replace the worker_key with the value from self.workers.
        For example:
        ```yaml
        workers:
         - tag worker_key
        ```
        is parsed as {'workers': WorkerLoader.workers[worker_key]}
        """
        worker_key = loader.construct_scalar(node)
        try:
            return self.workers[worker_key]
        except:
            raise KeyError(f'{worker_key} not present in workers.yml')


def parse_builders_yaml(file_name: str | Path, workers: dict) -> dict:
    """ Parse YAML builders file.

    :param file_name: File name.
    :param workers: Dict of valid workers.
    :return: config: File contents.
    """
    fname = Path(file_name)
    if not fname.is_file():
        raise FileNotFoundError(f'File not found: {fname.as_posix()}')

    worker_constructor = WorkerConstructor(workers)
    custom_loader = loader_factory(worker_constructor)

    with open(file_name, "r") as stream:
        try:
            config = yaml.load(stream, Loader=custom_loader)
        except yaml.YAMLError:
            raise yaml.YAMLError(f'Invalid formatting in YAML file: {fname.as_posix()}')

    return config
