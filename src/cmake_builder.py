"""CMake Builder class and assistant functions.
"""
from __future__ import annotations

import copy
from pathlib import Path
from typing import List, Optional

from buildbot.plugins import util, steps
from buildbot.config import BuilderConfig

from src.custom_steps import GitLabStep, BBGitStep
from src.yaml_spec import PackageManager, parse_builders_yaml


def spack_module_command(toolchain_name: str) -> str:
    """ Define Spack module command, for use with CMake.

    Notes
    -----------
    * Required as `mpsd-modules 23b` is not loadable from the buildbot shell.

    * Passing LDFLAGS as a key:value in env dict does not work, due to sub-shell processes.
      Hence, set as part of the module command.

    * For CMake we need to unset CPATH and LIBRARY_PATH. Otherwise, pkg-config sets CFLAGS
      that miss fortran include paths. This is valid for autotools, which uses CPATH and LIBRARY_PATH
      to determine them, but not CMake.

    * export PKG_CONFIG_ALLOW_SYSTEM_LIBS=1 and PKG_CONFIG_ALLOW_SYSTEM_CFLAGS=1
      could alternatively be used.

    :param toolchain_name: Module toolchain to load, i.e. `toolchains/foss2021a-serial`
    :return: modules_command: Full module command, with necessary export flags
    """
    module_cmd = '/usr/share/lmod/lmod/libexec/lmod'
    modules_command = f"eval `{module_cmd} sh use /opt_mpsd/linux-debian11/23b/sandybridge/lmod_cmake/Core` && "
    modules_command += f"eval `{module_cmd} sh load {toolchain_name}` && "
    # spack doesn't set rpath, so we need to set LDFLAGS manually
    modules_command += r"export LDFLAGS=`echo ${LIBRARY_PATH:+:$LIBRARY_PATH} | sed -e 's/:/ -Wl,-rpath=/g'` && "
    modules_command += r"export BLAS_LIB_DIR=$MPSD_OPENBLAS_ROOT/lib &&"
    modules_command += "unset CPATH && unset LIBRARY_PATH"
    return modules_command


class CmakeBuilderConfig:
    """ Class for parsing and processing the cmakebuilders.yml config file.

    Example Usage
    --------------
    workers = yaml_load("workers.yml")
    cmake_config = CmakeBuilderConfig(workers, file_name="cmake_builders.yaml")
    cmake_builders: dict = cmake_config.builders
    for name, settings in cmake_builders.items():
        c['builders'].append(octopus_cmake_builder(name, **settings)
    """
    default_file_name = Path("cmake_builders.yml")

    def __init__(self, workers: dict, file_name: Optional[str | Path] = None):
        """ Parse YAML builders configuration file.

        :param workers: Dict of valid workers
        :param file_name: YAML builders file name.
        :param perform_checks: Perform checks on flags and builders.
        """
        self.file_name = self.default_file_name if file_name is None else Path(file_name)
        self._config = self._parse_yaml(workers)
        self.defaults = self.preprocess_defaults()
        # Ensure values are strings for env dict
        self.defaults['env'] = {key: str(value) for key, value in self.defaults['env'].items()}
        self.builders = self.set_builders()

    def preprocess_defaults(self) -> dict:
        """ Preprocess Default fields.

        :return: defaults: Defaults with specific fields processed
        """
        defaults = self._config.pop('defaults')

        flatten = lambda l: sum(map(flatten, l), []) if isinstance(l, list) else [l]
        defaults['workers'] = flatten(defaults['workers'])

        # TODO(Alex) Issue 47. Move to YAML custom constructor
        pm_str = defaults['module']['package_manager'].split('.')[-1]
        defaults['module']['package_manager'] = PackageManager[pm_str]

        return defaults

    def _parse_yaml(self, workers: dict) -> dict:
        """ Parse YAML builders file.

        :param workers: Dict of valid workers
        :return: config: File contents.
        """
        return parse_builders_yaml(self.file_name, workers)

    def set_builders(self) -> dict:
        """ Set builders dictionary

        :return: builders: Dict of builders
        """
        # Recursively flatten nested list
        flatten = lambda l: sum(map(flatten, l), []) if isinstance(l, list) else [l]

        builders = {}
        for name, settings in self._config.items():
            # Initialise
            builders[name] = copy.deepcopy(self.defaults)

            # TODO(Alex) Issue 47. Move to YAML custom constructor
            # Ensure values are strings for env sub-dict
            if settings.get('env', False):
                settings['env'] = {key: str(value) for key, value in settings['env'].items()}

            # Flatten list (assume it's possible in WorkerConstructor, but no idea how)
            if settings.get('workers', False):
                settings['workers'] = flatten(settings['workers'])

            # TODO(Alex) Issue 47. Move to YAML custom constructor
            if settings.get('module', {}).get('package_manager', False):
                pm_str = settings['module']['package_manager'].split('.')[-1]
                settings['module']['package_manager'] = PackageManager[pm_str]

            # Fill with user-defined values
            for key, values in settings.items():
                # Replace defaults only when defined in settings
                if isinstance(values, dict):
                    builders[name][key].update(**values)
                # Always replace when defined in settings
                else:
                    builders[name][key] = values

        return builders


def octopus_cmake_builder(build_name: str,
                          preset: str,
                          workers: str | List[str],
                          module: dict,
                          env: dict,
                          test: dict,
                          run_test=False
                          ) -> BuilderConfig:
    """Set up an Octopus build for CMake, hard-coded for Spack module base.

    :param build_name: Unique builder name.
    :param preset: Cmake preset string.
    :param workers: Workers to run the pipeline on.
    :param module: Dict of module steps/commands.
    :param env: Environment variables.
    :param test: Test variables.
    TODO(Alex) Short-lived variable - remove as soon as ctest runs with no failures
    :param run_test: Run ctest

    :return: A CMake-based builder configuration instance.
    """
    package_manager = module.get('package_manager', None)
    assert package_manager is not None, 'module.package_manager must be specified for a CMake builder'
    if package_manager:
        assert package_manager == PackageManager.SPACK, 'CMake builders only implemented to be consistent with spack envs'

    my_env = env
    cmake_workers = workers if isinstance(workers, list) else [workers]
    module_cmd = spack_module_command(module['load'])

    # Define build factory
    build_factory = util.BuildFactory()
    build_factory.addStep(GitLabStep)
    build_factory.addStep(BBGitStep)

    # The chmod is necessary because files are left read-only if a distcheck fails.
    build_factory.addStep(steps.ShellCommand(description="Removing existing _build directory",
                                             descriptionDone="Clean",
                                             command="if [ -d _build ] ; then chmod -R +w _build && rm -rf _build; fi",
                                             haltOnFailure=True))

    build_factory.addStep(steps.ShellCommand(description="Update submodules",
                                             descriptionDone="Submodules Updated",
                                             command=f"git submodule update --init --recursive",
                                             haltOnFailure=True))

    build_factory.addStep(steps.ShellCommand(description="CMake Configuration",
                                             name='cmake configure',
                                             descriptionDone="cmake workflow",
                                             command=module_cmd + f" && cmake  --preset {preset} --fresh",
                                             haltOnFailure=True))

    build_factory.addStep(steps.ShellCommand(description="Cmake Building",
                                             name='cmake build',
                                             descriptionDone="cmake workflow",
                                             command=module_cmd + f" && cmake --build --preset {preset}",
                                             haltOnFailure=True))

    if run_test:
        build_factory.addStep(steps.ShellCommand(description="Cmake Testing",
                                                 name='ctest',
                                                 descriptionDone="cmake workflow",
                                                 command=module_cmd + f" && ctest --preset {preset}",
                                                 haltOnFailure=True))

    # TODO(Ashwin/Alex). Issu 48. Add Test Cases for CMake Foss 2021a_min_serial Builder

    return util.BuilderConfig(name=build_name, workernames=cmake_workers, factory=build_factory, env=my_env)
