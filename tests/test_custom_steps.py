from buildbot.plugins import util

from src.autotools_builder import run_tests
from src.custom_steps import OctopusTestingStep


def test_octopus_report_upload_step():
    """Initialisation of OctopusTestingStep class"""

    module_command = "eval `lmod sh use $EASYBUILD_PREFIX/foss-2020b_mpi/modules/Core ` && " \
                     "eval `lmod sh load foss Octopus/full ` && "
    test = 'short'
    test_command = 'cd _build && make short'
    use_valgrind = False
    timeout = 100

    step = OctopusTestingStep(module_command,
                                   test,
                                   test_command=test_command,
                                   timeout=timeout,
                                   doStepIf=run_tests)

    assert isinstance(step.command, util.Interpolate)
    assert step.command == util.Interpolate('eval `lmod sh use $EASYBUILD_PREFIX/foss-2020b_mpi/modules/Core ` '
                                            '&& eval `lmod sh load foss Octopus/full ` && cd _build && make short')
    assert step.env == {}, "env variables not set for standard test"
