import pytest

from src.dict_utils import get_nested_value, set_nested_value, iterate_over_immutable_values


def test_get_nested_value():
    combined_keys = ['flags', 'eb', 'configure_flags_min']
    d = {'flags': {'eb': {'configure_flags_min': 'my_flag'}}}
    value = get_nested_value(d, combined_keys)
    assert value == 'my_flag'


def test_get_nested_value_not_in_dict():
    combined_keys = ['flags', 'eb', 'configure_flags_min']
    d = {'flags': {'spack': {'configure_flags_min': 'my_flag'}}}

    with pytest.raises(KeyError) as error:
        value = get_nested_value(d, combined_keys)
    assert error.value.args[0] == 'Key sequence: [flags][eb] not present in dictionary'


def test_set_nested_value():
    input = {'a': {'b': {'c': False}}}
    keys = ['a', 'b', 'c']
    value = True

    output = set_nested_value(input, keys, value)
    reference = {'a': {'b': {'c': True}}}

    assert output == reference


def test_set_nested_value_not_in_dict():
    input = {'a': {'b': {'c': False}}}

    with pytest.raises(KeyError) as error:
        output = set_nested_value(input, ['a', 'c'], True)
    assert error.value.args[0] == 'Key sequence: [a][c] not present in dictionary', 'Some key not present'

    with pytest.raises(KeyError) as error:
        output = set_nested_value(input, ['a', 'b', 'd'], True)
    assert error.value.args[0] == 'Key sequence: [a][b][d] not present in dictionary', 'Last key not present'


def test_iterate_over_immutable_values():
    d = {'a': {'b': {'c': False, 'd': False}}}

    keys_of_false_values = []

    def log_false(key, value):
        if not value:
            keys_of_false_values.append(key)

    iterate_over_immutable_values(d, log_false)
    assert keys_of_false_values == [['a', 'b', 'c'], ['a','b', 'd']], 'Expect these nested keys to lead to false values'
